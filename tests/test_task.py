import time
import unittest
import json

from tasklist import Task


class TestTask(unittest.TestCase):
  def setUp(self):
    self.cmd_ok = ['python', '-c', 'import sys; sys.exit(0)']
    self.tok = Task(self.cmd_ok[0], args=self.cmd_ok[1:])
    self.cmd_bad = ['python', '-c', 'import sys; sys.exit(1)']
    self.tbad = Task(self.cmd_bad[0], args=self.cmd_bad[1:])
    self.cmd_temp = ['python', '-c', 'import sys; sys.exit(2)']
    self.ttemp = Task(self.cmd_temp[0], args=self.cmd_temp[1:])

  def test_build_list(self):
    t = Task(self.cmd_ok)

  def test_run(self):
    # does not raise
    self.tok.run()

  def test_run_ok(self):
    self.assertTrue(self.tok.run())
  
  def test_run_bad(self):
    self.assertFalse(self.tbad.run())
  
  def test_succeeded(self):
    self.tok.run()
    self.assertTrue(self.tok.succeeded())
    with self.assertRaises(RuntimeError):
      t = Task('true')
      self.assertFalse(t.succeeded())

  def test_failed_permanent(self):
    self.tbad.run()
    self.assertFalse(self.tbad.failed_retry())
    self.tok.run()
    self.assertFalse(self.tok.failed_retry())
  
  def test_failed_temporary(self):
    self.ttemp.run()
    self.assertTrue(self.ttemp.failed_retry())

  def test_str(self):
    self.assertNotEqual(str(self.tok), str(self.tbad))
  
  def test_repr(self):
    self.assertNotEqual(repr(self.tok), repr(self.tbad))

  def test_age(self):
    cur_age = self.tok.age()
    self.assertGreater(cur_age, 0.0)
    self.assertGreater(self.tok.age(), cur_age)

  def test_status(self):
    self.tok.run()
    self.assertTrue(self.tok.succeeded())
    self.assertLessEqual(self.tok.status['startts'], time.time())
    self.assertGreaterEqual(self.tok.status['runtime_s'], 0)

  def test_serialize(self):
    self.tok.run()
    ser = self.tok.serialize()
    self.assertNotEqual(len(ser), 0)
    self.assertIsInstance(json.loads(ser), dict)
  
  def test_deserialize(self):
    self.tok.run()
    ser = self.tok.serialize()
    t = Task.from_bytes(ser)
    self.assertIsNotNone(t)
    self.assertEqual(t, self.tok)
  
  def test_equals(self):
    self.assertEqual(self.tok, self.tok)
    self.assertNotEqual(self.tok, self.tbad)

  def test_get_cmd(self):
    self.assertEqual(self.cmd_ok, self.tok.get_cmd())
    self.assertEqual(self.cmd_bad, self.tbad.get_cmd())

  def test_get_stdin(self):
    self.assertIsNone(self.tok.get_stdin())
    stdin = 'foobar'
    t = Task('true', stdin=stdin)
    self.assertEqual(stdin, t.get_stdin())

  def test_get_env(self):
    self.assertEqual(dict(), self.tok.get_env())
    env = {'foo': 'bar'}
    t = Task(cmd='true', env=env)
    self.assertEqual(env, t.get_env())

  def test_get_cwd(self):
    self.assertIsNone(self.tok.get_cwd())
    cwd = '/tmp'
    t = Task(cmd='true', cwd=cwd)
    self.assertEqual(cwd, t.get_cwd())
