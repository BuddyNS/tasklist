import time
import unittest
import json

from tasklist import Task, TaskList


class TestTaskManager(unittest.TestCase):
  def setUp(self):
    self.wrkdir = './tasklist_wrkdir'
    self.tm = TaskList(wrkdir=self.wrkdir)
  
  def get_task(self, success=True, retriable=False, args=False, name=None):
    cmd = 'python'
    if success:
      xname = 'good'
      cmdargs = ['-c', 'import sys; sys.exit(0)']
    elif retriable:
      xname = 'bad-retriable'
      cmdargs = ['-c', 'import sys; sys.exit(2)']
    else:
      xname = 'bad-permanent'
      cmdargs = ['-c', 'import sys; sys.exit(1)']
    return Task(cmd, cmdargs, name=name or xname)

  def test_len(self):
    self.tm.clear()
    self.assertEqual(len(self.tm), 0)
    self.tm.add(self.get_task(True))
    self.assertEqual(len(self.tm), 1)
  
  def test_clear(self):
    self.tm.add(self.get_task(True))
    self.assertNotEqual(len(self.tm), 0)
    self.tm.clear()
    self.assertEqual(len(self.tm), 0)

  def test_clear_outdated(self):
    t = TaskList(wrkdir=self.wrkdir)
    t.add(self.get_task())
    t.add(self.get_task())
    t.add(self.get_task())
    self.assertEquals(3, len(t))
    t.clear_outdated(1)
    self.assertEquals(3, len(t))
    t.clear_outdated(0)
    self.assertFalse(t)
    with self.assertRaises(ValueError):
      t.clear_outdated(-1)
    with self.assertRaises(ValueError):
      t.clear_outdated('x')

  def test_run_next_good(self):
    self.tm.clear()
    self.tm.add(self.get_task(True))
    self.assertTrue(self.tm.run_next())

  def test_run_next_bad(self):
    self.tm.clear()
    self.tm.add(self.get_task(False))
    self.assertFalse(self.tm.run_next())
  
  def test_run_all_good(self):
    self.tm.clear()
    self.tm.add(self.get_task(True))
    self.tm.add(self.get_task(True))
    self.assertTrue(self.tm.run_all())
  
  def test_run_all_bad(self):
    self.tm.clear()
    self.tm.add(self.get_task(True))
    self.tm.add(self.get_task(False))
    self.assertFalse(self.tm.run_all())
  
  def test_run_all_tempfail_skip(self):
    self.tm.clear()
    self.tm.add(self.get_task(False, retriable=True))
    self.tm.add(self.get_task(True))
    self.assertFalse(self.tm.run_all(skip_permanent_failures=True))
    self.assertEqual(len(self.tm), 2)
    self.tm.clear()
    self.tm.add(self.get_task(False))
    self.tm.add(self.get_task(True))
    self.assertTrue(self.tm.run_all(skip_permanent_failures=True))
    self.assertEqual(len(self.tm), 0)

  def test_run_all_tempfail_noskip(self):
    self.tm.clear()
    self.tm.add(self.get_task(False, retriable=True))
    self.tm.add(self.get_task(True))
    self.assertFalse(self.tm.run_all(skip_permanent_failures=False))
    self.assertEqual(len(self.tm), 2)

  def test_serialize(self):
    self.tm.clear()
    self.tm.add(self.get_task(True, name='one'))
    self.tm.add(self.get_task(False, name='two'))
    self.assertEqual(len(self.tm), 2)
    self.tm.serialize()
    # load
    n = len(self.tm)
    newtm = TaskList(wrkdir=self.wrkdir)
    newtm.load()
    self.assertEqual(len(newtm), len(self.tm))
    self.assertEqual(self.tm, newtm)

  def test_autosync(self):
    newtm = TaskList(wrkdir=self.wrkdir, autosync=True)
    newtm.clear()
    self.assertEqual(len(newtm), 0)
    del newtm
    newtm = TaskList(wrkdir=self.wrkdir, autosync=True)
    self.assertEqual(len(newtm), 0)
    newtm.add(self.get_task(True))
    del newtm
    newtm = TaskList(wrkdir=self.wrkdir, autosync=True)
    self.assertEqual(len(newtm), 1)
  
  def test_age(self):
    newtm = TaskList(wrkdir=self.wrkdir)
    self.assertEqual(newtm.age(), 0)
    newtm.add(self.get_task(True))
    # age is real
    a = newtm.age()
    self.assertGreater(a, 0)
    # age grows
    b = newtm.age()
    self.assertGreater(b, a)
    # age shrinks when removing old items
    newtm.add(self.get_task(False))
    c = newtm.age()
    self.assertGreater(c, a)
    newtm.popleft()
    self.assertLess(newtm.age(), c)
  
  def test_status(self):
    newtm = TaskList(wrkdir=self.wrkdir)
    self.assertTrue(newtm.succeeded())
    newtm.add(self.get_task(True))
    with self.assertRaises(RuntimeError):
      newtm.succeeded()
    newtm.run_one()
    self.assertEqual(newtm.succeeded(), True)
    self.assertEqual(newtm.failed_retry(), False)
    newtm.clear()
    newtm.add(self.get_task(False))
    newtm.run_one()
    self.assertEqual(newtm.succeeded(), False)
    self.assertEqual(newtm.failed_retry(), False)
    newtm.clear()
    newtm.add(self.get_task(False, retriable=True))
    newtm.run_one()
    self.assertEqual(newtm.succeeded(), False)
    self.assertEqual(newtm.failed_retry(), True)
  
  def test_bool(self):
    tl = TaskList(wrkdir=self.wrkdir)
    self.assertFalse(tl)
    tl.add(self.get_task(False))
    self.assertTrue(tl)
    tl.add(self.get_task(True))
    self.assertTrue(tl)

  def test_iteration(self):
    tl = TaskList(wrkdir=self.wrkdir)
    tl.add(self.get_task(False))
    tl.add(self.get_task(True))
    tasks = [t for t in tl]
    self.assertEqual(2, len(tasks))
