# Running tests

1. Download (or clone) repository
1. Change to directory `tests`
1. Run the commands below

## For Python 3 tests

```
# cd tests

# python3 tests
PYTHONPATH=../src python3 -m unittest test_task
PYTHONPATH=../src python3 -m unittest test_tasklist
```

## For python 2 tests

```
# cd tests

# python2 tests
PYTHONPATH=../src python2 -m unittest test_task
PYTHONPATH=../src python2 -m unittest test_tasklist
```